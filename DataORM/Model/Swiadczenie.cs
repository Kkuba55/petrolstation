﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Swiadczenie
    {
        [Key]
        public int ID_SWI { get; set; }

        //[Column(TypeName = "DOUBLE(7)")]
        [Required]
        public double Pensja { get; set; }

       //TODO: ID_PRA KLUCZ OBCY
       
       public int PracownikId { get; set; }
        [ForeignKey("PracownikId")]
        public Pracownik Pracownik { get; set; }
    }
}
