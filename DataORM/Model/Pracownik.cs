﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Pracownik
    {
        [Key]
        public int ID_PRA { get; set; }

        //[Column(TypeName = "VARCHAR(15)")]
        [Required]
        public string Imie { get; set; }

        //[Column(TypeName = "VARCHAR(15)")]
        [Required]
        public string Nazwisko { get; set; }


        [Required]
        public string Pesel { get; set; }


        [Required]
        public bool CzyUprawniony { get; set; }

        [Required]
        public bool CzyUsuniety { get; set; } = false;

        // TODO: Add FK Adres
        public int AdresId { get; set; }
        [ForeignKey("AdresId")]
        public virtual Adres Adres { get; set; }

       // [ForeignKey("Swiadczenie")]
        public ICollection<Swiadczenie> Swiadczenies { get; set; }

      // [ForeignKey("Faktura")]
        public ICollection<Faktura> Fakturas { get; set; }


    }
}
