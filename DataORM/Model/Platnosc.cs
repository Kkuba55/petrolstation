﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Platnosc
    {
        [Key]
        public int ID_PLA { get; set; }

        //[Column(TypeName = "VARCHAR(10)")]
        [Required]
        public string Typ { get; set; }

        [Required]
        public string Kwota { get; set; }
    }
}
