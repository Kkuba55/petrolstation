﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Rezerwacja
    {
        [Key]
        public int ID_REZ { get; set; }

        //[Column(TypeName = "TIMESTAMP")]
        [Required]
        public DateTime Data { get; set; }

        
        // TODO: Add KLI_ID, PRO_ID FK
        
        public int KlientId { get; set; }
        [ForeignKey("KlientId")]
        public Klient Klient { get; set; }
    }
}
