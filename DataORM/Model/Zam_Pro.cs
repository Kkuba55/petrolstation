﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Zam_Pro
    {
        [Key]
        public int ID_ZAM_PRO { get; set; }

        //TODO ZAM_ID FK


        //TODO PRO_ID FK
        [ForeignKey("Produkt")]
        public ICollection<Produkt> Produkts { get; set; }
    }
}
