﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Stawka_VAT
    {
        [Key]
        public int ID_STA { get; set; }

        //[Column(TypeName = "DOUBLE(7)")]
        [Required]
        public double Stawka { get; set; }

    }
}
