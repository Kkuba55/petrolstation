﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Karta_Loj
    {
        [Key]
        public int ID_KAR { get; set; }

        [Required]
        public int LiczbaPunktow { get; set; } = 0;
       
        public int ID_KLI { get; set; }
        [Required, ForeignKey("ID_KLI")]
        public virtual Klient Klient { get; set; }

    }
}
