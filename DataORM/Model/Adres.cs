﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Adres
    {
        [Key]
        public int ID_ADR { get; set; }

        //[Column(TypeName = "VARCHAR(15)")]
        [Required]
        public string Ulica { get; set; }

        [Required]
        public string Kod_pocztowy { get; set; }

        //[Column(TypeName = "VARCHAR(14)")]
        [Required]
        public string Miasto { get; set; }

        [Required]
        public string Numer_domu { get; set; }

      //  [ForeignKey("Klient")]
        public virtual ICollection<Klient> Klients { get; set; }

       //[ForeignKey("Pracownik")]
        public virtual ICollection<Pracownik> Pracowniks { get; set; }
    }
}
