﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Produkt
    {
        [Key]
        public int ID_PRO { get; set; }

        [Required]
        public double Cena { get; set; } = 0.00;


        [Required]
        public string Nazwa { get; set; }


        public double StawkaVat { get; set; } = 1.23;

        
        [Required]
        public int Ilosc { get; set; }

        //TODO: FK STA_ID 
       [ForeignKey("Zam_Pro")]
        public ICollection<Zam_Pro> Zam_Pros { get; set; }

       [ForeignKey("Faktura")]
        public ICollection<Faktura> Fakturas { get; set; }
    }
}
