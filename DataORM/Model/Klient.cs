﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Klient
    {
        [Key]
        public int ID_KLI { get; set; }

        //[Column(TypeName = "VARCHAR(15)")]
        [Required]
        public string Imie { get; set; }

        //[Column(TypeName = "VARCHAR(20)")]
        [Required]
        public string Nazwisko { get; set; }


        [Required]
        // [Column(TypeName = "LONGTEXT(11)")]
        //[Index(IsUnique = true)]
        public string Pesel { get; set; }

        [Required]
        public bool CzyUsuniety { get; set; } = false;

        public int AdresId { get; set; }
        [ForeignKey("AdresId")]
        public virtual Adres Adres { get; set; }


        public int ID_KAR { get; set; }
        [ForeignKey("ID_KAR")]
        public virtual Karta_Loj Karta_Loj { get; set; }

       
        //[ForeignKey("Faktura")]
        public ICollection<Faktura> Fakturas { get; set; }

       // [ForeignKey("Rezerwacja")]
        public ICollection<Rezerwacja> Rezerwacjas { get; set; }

    }
}
