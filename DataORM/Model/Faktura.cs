﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataORM.Model
{
    public class Faktura
    {
        [Key]
        public int ID_FAK { get; set; }


        [Required]
        public double Kwota_brutto { get; set; }

        [Required]
        public double Kwota_netto { get; set; }

        //[Column(TypeName = "VARCHAR(12)")]
        [Required]
        public string Numer_faktury { get; set; }

        // TODO: KLI_ID and PRA_ID for.key
        
        public int? KlientId { get; set; }
        [ForeignKey("KlientId")]
        public Klient Klient { get; set; }

       
        public int? PracownikId { get; set; }
        [ForeignKey("PracownikId")]
        public Pracownik Pracownik { get; set; }

        [ForeignKey("Produkt")]
        public virtual ICollection<Produkt> Produkts { get; set; }
    }
}
