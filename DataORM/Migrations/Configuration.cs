namespace DataORM.Migrations
{
    using DataORM.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataORM.PetrolStationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            SetSqlGenerator("MySql.Data.MySqlClient", new myMigrationSQLGenerator());
        }

        protected override void Seed(DataORM.PetrolStationContext context)
        {
          // //  This method will be called after migrating to the latest version.
          //
          // //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
          // //  to avoid creating duplicate seed data.
          //
          // var benzyna = new Produkt()
          // {
          //     Nazwa = "PB95",
          //     Ilosc = 20,
          //     Cena = 4.5
          // };
          //
          // var diesel = new Produkt()
          // {
          //     Nazwa = "ON",
          //     Ilosc = 20,
          //     Cena = 5
          // };
          //
          ///  context.Produkts.Add(benzyna);
          ///  context.Produkts.Add(diesel);
          ///  context.SaveChanges();
          // //
          // //OdejmijProdukt(benzyna);
          //
          //   var p = context.Pracowniks.Where(x => x.Pesel == "12312312312").FirstOrDefault();
          //
         ///   Pracownik prac1 = new Pracownik()
         ///   {
         ///       Imie = "Jan",
         ///       Nazwisko = "Kukulski",
         ///       Pesel = "12312312312",
         ///       CzyUprawniony = true,
         ///       Adres = new Adres()
         ///       {
         ///           Miasto = "Krakow",
         ///           Ulica = "Orlinskiego",
         ///           Kod_pocztowy = "13-133",
         ///           Numer_domu = "13"
         ///       }
         ///   };
         /////
          ///  context.Pracowniks.Add(prac1);
          ///  context.SaveChanges();
          ///
          //
          //ZamowProdukt(p, new List<Produkt>() { benzyna, diesel });
          //
          //context.SaveChanges();
          //
          //
        }

        //public void KupProdukt(Klient klient, ICollection<Produkt> produkty)
        //{
        //    var kupioneProdukty = new List<Produkt>();

        //    foreach (var produkt in produkty)
        //    {
        //        if (OdejmijProdukt(produkt))
        //            kupioneProdukty.Add(produkt);
        //    }

        //    var faktura = WystawFakture(kupioneProdukty);
        //    faktura.Klient = klient;
        //    using (var context = new PetrolStationContext())
        //    {
        //        context.Fakturas.Add(faktura);
        //        context.SaveChanges();
        //    }
        //}

        public void ZamowProdukt(Pracownik pracownik, ICollection<Produkt> produkty)
        {
            using (var context = new PetrolStationContext())
            {
                var dbp = new List<Produkt>();

                foreach (var produkt in produkty)
                {
                    DodajProdukt(produkt);
                    var p = context.Produkts.Where(x => x.Nazwa == produkt.Nazwa).FirstOrDefault();
                    p.Ilosc += produkt.Ilosc;
                    dbp.Add(p);
                }



                var faktura = new Faktura()
                {

                    Produkts = dbp,
                    Numer_faktury = "FV" + new Random().Next(1, 100000),
                    Kwota_netto = produkty.Sum(x => x.Cena * x.Ilosc),
                    Kwota_brutto = produkty.Sum(x => x.Cena * x.Ilosc * x.StawkaVat)
                };

                faktura.Pracownik = context.Pracowniks.Where(x => x.Pesel == pracownik.Pesel).FirstOrDefault();

                context.Fakturas.Add(faktura);
                context.SaveChanges();
            }
        }

        public void DodajProdukt(Produkt zakup)
        {
            using (var context = new PetrolStationContext())
            {



                context.SaveChanges();
            }
        }

        public bool OdejmijProdukt(Produkt zakup)
        {
            using (var context = new PetrolStationContext())
            {
                var produkt = context.Produkts.Where(x => x.Nazwa == zakup.Nazwa).FirstOrDefault();

                if (produkt.Ilosc - zakup.Ilosc > 0)
                {
                    produkt.Ilosc -= zakup.Ilosc;
                    context.SaveChanges();

                    return true;
                }

                return false;
            }
        }

        //public Faktura WystawFakture(ICollection<Produkt> zakup)
        //{
        //    using (var context = new PetrolStationContext())
        //    {
        //        var dbp = new List<Produkt>();
        //        foreach (var p in zakup) dbp.Add(context.Produkts.Where(x => x.Nazwa == p.Nazwa).FirstOrDefault());

        //        var faktura = new Faktura()
        //        {
        //            Produkts = dbp,
        //            Numer_faktury = "FV" + new Random().Next(1, 100000),
        //            Kwota_netto = dbp.Sum(x => x.Cena * x.Ilosc),
        //            Kwota_brutto = dbp.Sum(x => x.Cena * x.Ilosc * x.StawkaVat)
        //        };

        //        return faktura;
        //    } 
        //}
    }
}
