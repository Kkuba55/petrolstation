﻿using System;
using System.Data.Entity;
using DataORM.Model;
using MySql.Data.EntityFramework;

namespace DataORM
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class PetrolStationContext : DbContext
    {
        public DbSet<Adres> Adresses { get; set; }
        public DbSet<Faktura> Fakturas { get; set; }
        public DbSet<Karta_Loj> Karta_Lojs { get; set; }
        public DbSet<Klient> Klients { get; set; }
        public DbSet<Platnosc> Platnoscs { get; set; }
        public DbSet<Pracownik> Pracowniks { get; set; }
        public DbSet<Produkt> Produkts { get; set; }
        public DbSet<Program_mycia> Program_Mycias { get; set; }
        public DbSet<Rezerwacja> Rezerwacjas { get; set; }
        public DbSet<Stawka_VAT> Stawka_VATs { get; set; }
        public DbSet<Swiadczenie> Swiadczenies { get; set; }
        public DbSet<Tankowanie> Tankowanies { get; set; }
        public DbSet<Zam_Pro> Zam_Pros { get; set; }
        public DbSet<Zamowienie> Zamowienies { get; set; }

        public PetrolStationContext(): 
            base("server=remotemysql.com;port=3306;user id=urOY0IbW9N;database=urOY0IbW9N;password=k3fyEFzs0x")
        { 
            
        }
    }
}
