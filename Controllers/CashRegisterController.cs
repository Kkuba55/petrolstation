﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataORM.Model;
using DataORM;

namespace ProjZes.Controllers
{
    public class CashRegisterController : Controller
    {
        public ActionResult Index()
        {
            using(var context = new DataORM.PetrolStationContext()) {
                var products = context.Produkts.ToList();

                var projzesProducts = new List<Models.Product>();

                foreach(var product in products)
                {
                    projzesProducts.Add(new Models.Product() {
                        Cena = $"{product.Cena}".Replace(",", "."),
                        Ilosc = product.Ilosc,
                        Nazwa = product.Nazwa,
                        StawkaVat = product.StawkaVat
                    });
                }

                return View("~/Views/CashRegister/CashRegister.cshtml", projzesProducts);
            }


        }

        public ActionResult AddInvoice(FormCollection form) { 
            using(var context = new DataORM.PetrolStationContext()) { 

            }

            return Index();
        }

        [HttpPost]
        public ActionResult SaveList(IEnumerable<KupionyProdukt> postData)
        {

            Console.Write(postData.FirstOrDefault().Nazwa, postData.FirstOrDefault().Ilosc);

            var kupioneProdukty = new List<Produkt>();

            foreach(var produkt in postData) {
                kupioneProdukty.Add(new Produkt() {
                    Nazwa = produkt.Nazwa,
                    Ilosc = produkt.Ilosc
                });
            }

            KupProdukty(kupioneProdukty);

            return View("~/Views/CashRegister/CashRegister.cshtml");
        }

        public void KupProdukty(ICollection<Produkt> produkty)
        {
            using (var context = new PetrolStationContext())
            {
                var dbp = new List<Produkt>();
                double netto = 0;
                double brutto = 0;

                foreach (var produkt in produkty)
                {
                    var p = context.Produkts.Where(x => x.Nazwa.Replace(" ", "") == produkt.Nazwa.Replace(" ", "")).FirstOrDefault();
                    p.Ilosc -= produkt.Ilosc;
                    netto += produkt.Ilosc * p.Cena;
                    brutto += produkt.Ilosc * p.Cena * p.StawkaVat;
                    dbp.Add(p);
                }

                var faktura = new Faktura()
                { 
                    Produkts = dbp,
                    Numer_faktury = "FV" + new Random().Next(1, 100000),
                    Kwota_netto = netto,
                    Kwota_brutto = brutto
                };

                //faktura.Pracownik = context.Pracowniks.Where(x => x.Pesel == pracownik.Pesel).FirstOrDefault();

                context.Fakturas.Add(faktura);
                context.SaveChanges();
            }
        }
    }

    public class KupionyProdukt { 
        public int Ilosc { get; set; }
        public string Nazwa { get; set; }
    }


}
