﻿using DataORM;
using DataORM.Model;
using ProjZes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjZes.Controllers
{
    public class WorkersController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Views/Workers/Index.cshtml");
        }

        [HttpPost]
        public ActionResult AddWorker(FormCollection form)
        {
            using (var context = new PetrolStationContext())
            {
                var worker = new DataORM.Model.Pracownik()
                {
                    Imie = form["userName"],
                    Nazwisko = form["userLastName"],
                    Pesel = form["pesel"],
                    CzyUprawniony = false,
                    Adres = new Adres()
                    {
                        Miasto = form["city"],
                        Ulica = form["street"],
                        Numer_domu = form["number"],
                        Kod_pocztowy = form["zipCode"]
                    }


                };
                if (form["uprawniony"] == "uprawniony")
                {
                    worker.CzyUprawniony = true;
                }

                context.Pracowniks.Add(worker);

                context.SaveChanges();
            }
            return WorkerManagement();
        }
        
        [HttpGet]
        public ActionResult WorkerManagement()
        {
            var workers = new List<Worker>();
            using (var context = new PetrolStationContext())
            {
                var pracownicy = context.Pracowniks.Where(x => x.CzyUsuniety == false).ToList();

                foreach (var worker in pracownicy)
                {
                    workers.Add(new Worker()
                    {
                        Pesel = worker.Pesel,
                        Name = worker.Imie,
                        LastName = worker.Nazwisko,
                        IsApproved = worker.CzyUprawniony,
                        Miasto = worker.Adres.Miasto,
                        Ulica = worker.Adres.Ulica,
                        KodPocztowy = worker.Adres.Kod_pocztowy,
                        NrDomu = worker.Adres.Numer_domu
                    });
                }
            }
            return View("WorkerManagement", workers);
        }

        [HttpGet]
        public ActionResult DeleteWorker(string pesel)
        {
            using (var context = new PetrolStationContext())
            {

                context
                    .Pracowniks
                    .Where(x => x.Pesel == pesel.ToString())
                    .SingleOrDefault()
                    .CzyUsuniety = true;

                context.SaveChanges();
            }

            return WorkerManagement();
        }

        [HttpPost]
        public ActionResult EditWorker(FormCollection form)
        {
            using (var context = new PetrolStationContext())
            {
                var pesel = form["pesel"];

                var worker = context.Pracowniks.Where(x => x.Pesel == pesel).FirstOrDefault();
                worker.Imie = form["name"];
                worker.Nazwisko = form["lname"];
                worker.Pesel = form["pesel"];
                worker.CzyUprawniony = form["role_id"] == "Uprawniony" ? true : false;
                worker.Adres.Miasto = form["city"];
                worker.Adres.Ulica = form["street"];
                worker.Adres.Numer_domu = form["streetnumber"];
                worker.Adres.Kod_pocztowy = form["postcode"];

                context.SaveChanges();
            }
            return WorkerManagement();
        }


    }
}