﻿using DataORM;
using DataORM.Model;
using ProjZes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjZes.Controllers
{
    public class CustomersController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Views/Customers/Index.cshtml");
        }

        [HttpPost]
        public ActionResult AddCustomer(FormCollection form)
        {
            using (var context = new PetrolStationContext())
            {
                var client = new DataORM.Model.Klient()
                {
                    Imie = form["userName"],
                    Nazwisko = form["userLastName"],
                    Pesel = form["pesel"],
                    Adres = new Adres()
                    {
                        Miasto = form["city"],
                        Ulica = form["street"],
                        Kod_pocztowy = form["zipCode"],
                        Numer_domu = form["number"]
                    }

                };

                context.Klients.Add(client);

                
                context.SaveChanges();
            }
            return CustomerManagement();
        }

        [HttpGet]
        public ActionResult CustomerManagement()
        {
            var customers = new List<Customer>();
            using(var context = new PetrolStationContext())
            {
                var klienci = context.Klients.Where(x => x.CzyUsuniety == false).ToList();

                foreach (var customer in klienci)
                {
                    customers.Add(new Customer() {
                        Pesel = customer.Pesel,
                        Name = customer.Imie,
                        LastName = customer.Nazwisko,
                        Miasto = customer.Adres.Miasto,
                        Ulica = customer.Adres.Ulica,
                        KodPocztowy = customer.Adres.Kod_pocztowy,
                        NrDomu = customer.Adres.Numer_domu
                        
                    });
                }
            }

            return View("~/Views/Customers/CustomerManagement.cshtml", customers);
        }

        [HttpGet]
        public ActionResult DeleteCustomer(string pesel)
        {
            using (var context = new PetrolStationContext())
            {

                context
                    .Klients
                    .Where(x => x.Pesel == pesel.ToString())
                    .SingleOrDefault()
                    .CzyUsuniety = true;

                context.SaveChanges();
            }

            return CustomerManagement();
        }

        [HttpPost]
        public ActionResult EditCustomer(FormCollection form)
        {
            using (var context = new PetrolStationContext())
            {
                var pesel = form["pesel"];

                var customer = context.Klients.Where(x => x.Pesel == pesel).FirstOrDefault();
                customer.Imie = form["name"];
                customer.Nazwisko = form["lname"];
                customer.Pesel = form["pesel"];
                customer.Adres.Miasto = form["city"];
                customer.Adres.Ulica = form["street"];
                customer.Adres.Numer_domu = form["streetnumber"];
                customer.Adres.Kod_pocztowy = form["postcode"];

                context.SaveChanges();
            }
            return CustomerManagement();
        }

    }
}
