﻿using System;
namespace ProjZes.Models
{
    public struct Product
    {
        public string Cena { get; set; }
        public string Nazwa { get; set; }
        public double StawkaVat { get; set; }
        public int Ilosc { get; set; }
    }
}
