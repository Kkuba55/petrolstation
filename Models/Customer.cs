﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjZes.Models
{
    public class Customer
    {
        public string Name{ get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Pesel { get; set; }
        public string Ulica { get; set; }
        public string KodPocztowy { get; set; }
        public string Miasto { get; set; }
        public string NrDomu { get; set; }
    }
}